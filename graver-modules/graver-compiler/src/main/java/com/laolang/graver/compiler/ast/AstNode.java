package com.laolang.graver.compiler.ast;

/**
 * xxx.
 *
 * @author laolang
 * @version 0.1
 */
public interface AstNode {

    void accept(AstVisitor visitor);
}
