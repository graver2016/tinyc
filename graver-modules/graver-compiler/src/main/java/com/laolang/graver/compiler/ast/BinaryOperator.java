package com.laolang.graver.compiler.ast;

/**
 * xxx.
 *
 * @author laolang
 * @version 0.1
 */
public enum BinaryOperator {
    ADD,
    SUB,
    MUL,
    DIV
}
