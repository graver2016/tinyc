package com.laolang.graver.compiler;

import cn.hutool.core.util.StrUtil;
import com.laolang.graver.compiler.ast.AstProgramNode;
import com.laolang.graver.compiler.codegen.CodeGen;
import com.laolang.graver.compiler.lexer.Lexer;
import com.laolang.graver.compiler.parser.Parser;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;

/**
 * xxx.
 *
 * @author laolang
 * @version 0.1
 */
@Slf4j
public class Compiler {

    public void compile(String source, String outFile) {
        Lexer lexer = new Lexer(source);
        Parser parser = new Parser(lexer);
        CodeGen visitor = new CodeGen();
        AstProgramNode program = parser.parse();
        program.accept(visitor);
        if (StrUtil.isNotBlank(outFile)) {
            writeAssemblyFile(visitor.getAssemblyText(), outFile);
        }

    }

    private void writeAssemblyFile(String text, String assemblyFilePath) {
        // 创建 File 对象
        File file = new File(assemblyFilePath);

        try {
            // 判断文件是否存在，不存在则创建文件
            if (!file.exists()) {
                if (file.createNewFile()) {
                    System.out.println("输出文件:" + assemblyFilePath);
                } else {
                    System.out.println(StrUtil.format("文件:{} 创建失败", assemblyFilePath));
                }
            }

            // 写入文件内容为 "helloworld"
            FileWriter writer = new FileWriter(file);
            writer.write(text);
            writer.close();

            System.out.println("文件处理成功");

        } catch (IOException e) {
            System.out.println("文件处理失败:" + ExceptionUtils.getMessage(e));
        }
    }

}
