package com.laolang.graver.compiler.token;

import com.google.common.base.Strings;
import java.util.Objects;
import lombok.Data;

/**
 * xxx.
 *
 * @author laolang
 * @version 0.1
 */
@Data
public class Token {

    public Token(TokenKind kind, String text) {
        this(kind, null, text);
    }

    public Token(TokenKind kind, Integer value, String text) {
        this.kind = kind;
        this.value = value;
        this.text = text;
    }

    private TokenKind kind;
    private Integer value;
    private String text;

    public String toString() {
        String str = Strings.padEnd(kind.name(), 10, ' ');
        if (Objects.nonNull(text)) {
            str += ":" + Strings.padEnd(text, 5, ' ');
        }
        if (Objects.nonNull(value)) {
            str += ":" + value;
        }
        return str;
    }
}
