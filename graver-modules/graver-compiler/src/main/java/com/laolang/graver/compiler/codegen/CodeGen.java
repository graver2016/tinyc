package com.laolang.graver.compiler.codegen;

import cn.hutool.core.util.StrUtil;
import com.laolang.graver.compiler.ast.AstBinaryNode;
import com.laolang.graver.compiler.ast.AstConstantNode;
import com.laolang.graver.compiler.ast.AstProgramNode;
import com.laolang.graver.compiler.ast.AstVisitor;

/**
 * xxx.
 *
 * @author laolang
 * @version 0.1
 */
public class CodeGen implements AstVisitor {

    private int stackLevel = 0;
    private final StringBuilder buffer = new StringBuilder();

    public String getAssemblyText() {
        return buffer.toString();
    }

    @Override
    public void visitorProgramNode(AstProgramNode node) {
        System.out.println("\t.text");
        buffer.append("\t.text");
        buffer.append("\n");
        System.out.println("\t.globl prog");
        buffer.append("\t.globl prog");
        buffer.append("\n");
        System.out.println("prog:");
        buffer.append("prog:");
        buffer.append("\n");

        System.out.println("\tpush %rbp");
        buffer.append("\tpush %rbp");
        buffer.append("\n");
        System.out.println("\tmov %rsp, %rbp");
        buffer.append("\tmov %rsp, %rbp");
        buffer.append("\n");
        System.out.println("\tsub $32, %rsp");
        buffer.append("\tsub $32, %rsp");
        buffer.append("\n");

        node.getNode().accept(this);

        System.out.println("\tmov %rbp, %rsp");
        buffer.append("\tmov %rbp, %rsp");
        buffer.append("\n");
        System.out.println("\tpop %rbp");
        buffer.append("\tpop %rbp");
        buffer.append("\n");
        System.out.println("\tret");
        buffer.append("\tret");
        buffer.append("\n");
    }

    @SuppressWarnings("checkstyle:MissingSwitchDefault")
    @Override
    public void visitorBinaryNode(AstBinaryNode node) {
        node.getRight().accept(this);
        push();
        node.getLeft().accept(this);
        pop("%rdi");
        switch (node.getOp()) {
            case ADD:
                System.out.println("\tadd %rdi, %rax");
                buffer.append("\tadd %rdi, %rax");
                buffer.append("\n");
                break;
            case SUB:
                System.out.println("\tsub %rdi, %rax");
                buffer.append("\tsub %rdi, %rax");
                buffer.append("\n");
                break;
            case MUL:
                System.out.println("\timul %rdi, %rax");
                buffer.append("\timul %rdi, %rax");
                buffer.append("\n");
                break;
            case DIV:
                System.out.println("\tcqo");
                buffer.append("\tcqo");
                buffer.append("\n");
                System.out.println("\tidiv %rdi");
                buffer.append("\tidiv %rdi");
                buffer.append("\n");
                break;
        }

    }

    @Override
    public void visitorConstantNode(AstConstantNode node) {
        System.out.println(StrUtil.format("\tmov ${}, %rax", node.getValue()));
        buffer.append(StrUtil.format("\tmov ${}, %rax", node.getValue()));
        buffer.append("\n");
    }

    private void push() {
        System.out.println("\tpush %rax");
        buffer.append("\tpush %rax");
        buffer.append("\n");
        stackLevel++;
    }

    private void pop(String value) {
        System.out.println(StrUtil.format("\tpop {}", value));
        buffer.append(StrUtil.format("\tpop {}", value));
        buffer.append("\n");
        stackLevel--;
    }
}
