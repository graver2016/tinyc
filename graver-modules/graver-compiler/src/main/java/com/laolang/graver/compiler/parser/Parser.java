package com.laolang.graver.compiler.parser;

import com.laolang.graver.compiler.ast.AstBinaryNode;
import com.laolang.graver.compiler.ast.AstConstantNode;
import com.laolang.graver.compiler.ast.AstNode;
import com.laolang.graver.compiler.ast.AstProgramNode;
import com.laolang.graver.compiler.ast.BinaryOperator;
import com.laolang.graver.compiler.lexer.Lexer;
import com.laolang.graver.compiler.token.TokenKind;

/**
 * xxx.
 *
 * @author laolang
 * @version 0.1
 */
public class Parser {

    public Parser(Lexer lexer) {
        this.lexer = lexer;
        lexer.nextToken();
    }

    private final Lexer lexer;

    public AstProgramNode parse() {
        AstProgramNode program = new AstProgramNode();
        program.setNode(parseExpr());
        return program;
    }

    private AstNode parseExpr() {
        return parseAddExpr();
    }

    private AstNode parseAddExpr() {
        AstNode node = parseMultiExpr();
        while (TokenKind.ADD == lexer.getCurrToken().getKind() || TokenKind.SUB == lexer.getCurrToken().getKind()) {
            BinaryOperator op = BinaryOperator.ADD;
            if (TokenKind.SUB == lexer.getCurrToken().getKind()) {
                op = BinaryOperator.SUB;
            }
            lexer.nextToken();
            AstBinaryNode binary = new AstBinaryNode();
            binary.setOp(op);
            binary.setLeft(node);
            binary.setRight(parseMultiExpr());
            node = binary;
        }
        return node;
    }

    private AstNode parseMultiExpr() {
        AstNode node = parsePrimaryExpr();
        while (TokenKind.MUL == lexer.getCurrToken().getKind() || TokenKind.DIV == lexer.getCurrToken().getKind()) {
            BinaryOperator op = BinaryOperator.MUL;
            if (TokenKind.DIV == lexer.getCurrToken().getKind()) {
                op = BinaryOperator.DIV;
            }
            lexer.nextToken();
            AstBinaryNode binary = new AstBinaryNode();
            binary.setOp(op);
            binary.setLeft(node);
            binary.setRight(parsePrimaryExpr());
            node = binary;
        }
        return node;
    }

    private AstNode parsePrimaryExpr() {
        AstConstantNode constant = new AstConstantNode();
        constant.setValue(lexer.getCurrToken().getValue());
        lexer.nextToken();
        return constant;
    }


}
