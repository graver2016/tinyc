package com.laolang.graver.compiler.token;

/**
 * xxx.
 *
 * @author laolang
 * @version 0.1
 */
public enum TokenKind {

    EOF,
    BAD,
    ADD,
    SUB,
    MUL,
    DIV,
    NUM
}
