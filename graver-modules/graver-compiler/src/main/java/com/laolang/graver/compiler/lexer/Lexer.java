package com.laolang.graver.compiler.lexer;

import com.google.common.collect.Lists;
import com.laolang.graver.compiler.token.Token;
import com.laolang.graver.compiler.token.TokenKind;
import java.util.List;
import lombok.Data;

/**
 * xxx.
 *
 * @author laolang
 * @version 0.1
 */
@Data
public class Lexer {

    public Lexer() {
        this.cursor = 0;
        this.currChar = '\0';
        this.ops = Lists.newArrayList();
        ops.add('+');
        ops.add('-');
        ops.add('*');
        ops.add('/');
    }

    public Lexer(String source) {
        this();
        this.source = source;
        this.currChar = this.source.charAt(0);
    }

    private String source;
    private Token currToken;
    private char currChar;
    private Integer cursor;
    private final List<Character> ops;

    public void nextToken() {
        while (Character.isWhitespace(currChar)) {
            nextChar();
        }

        switch (currChar) {
            case '\0': {
                currToken = new Token(TokenKind.EOF, null);
                nextChar();
                break;
            }
            case '+': {
                currToken = new Token(TokenKind.ADD, "+");
                nextChar();
                break;
            }
            case '-': {
                currToken = new Token(TokenKind.SUB, "-");
                nextChar();
                break;
            }
            case '*': {
                currToken = new Token(TokenKind.MUL, "*");
                nextChar();
                break;
            }
            case '/': {
                currToken = new Token(TokenKind.DIV, "/");
                nextChar();
                break;
            }
            default: {
                if (Character.isDigit(currChar)) {
                    parseNumberToken();
                } else {
                    StringBuilder sb = new StringBuilder();
                    while ('\0' != currChar) {
                        sb.append(currChar);
                        nextChar();
                        boolean inOps = ops.contains(currChar);
                        if (Character.isWhitespace(currChar) || inOps || Character.isDigit(currChar)) {
                            break;
                        }
                    }
                    currToken = new Token(TokenKind.BAD, sb.toString());
                }
            }
        }
    }

    private void parseNumberToken() {
        StringBuilder sb = new StringBuilder();
        boolean validNumber = false;
        boolean startWithZero = '0' == currChar;
        while (Character.isDigit(currChar)) {
            sb.append(currChar);
            validNumber = Character.isDigit(currChar);
            nextChar();
            if (Character.isWhitespace(currChar)) {
                break;
            }
        }
        if (validNumber && !startWithZero) {
            currToken = new Token(TokenKind.NUM, Integer.parseInt(sb.toString()), sb.toString());
        } else {
            currToken = new Token(TokenKind.BAD, sb.toString());
        }
    }

    private void nextChar() {
        cursor++;
        currChar = peek();
    }

    private char peek() {
        return peek(0);
    }

    private char peek(int offset) {
        int index = cursor + offset;
        if (index >= source.length()) {
            return '\0';
        }
        return source.charAt(index);
    }
}
