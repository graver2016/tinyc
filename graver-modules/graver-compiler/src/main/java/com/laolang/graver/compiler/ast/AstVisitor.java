package com.laolang.graver.compiler.ast;

/**
 * xxx.
 *
 * @author laolang
 * @version 0.1
 */
public interface AstVisitor {

    void visitorProgramNode(AstProgramNode node);

    void visitorBinaryNode(AstBinaryNode node);

    void visitorConstantNode(AstConstantNode node);

}
