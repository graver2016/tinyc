package com.laolang.graver.compiler.ast;

/**
 * xxx.
 *
 * @author laolang
 * @version 0.1
 */
public class AstPrinterVisitor implements AstVisitor {

    @Override
    public void visitorProgramNode(AstProgramNode node) {
        node.getNode().accept(this);
        System.out.println();
    }

    @SuppressWarnings("checkstyle:MissingSwitchDefault")
    @Override
    public void visitorBinaryNode(AstBinaryNode node) {
        node.getRight().accept(this);
        node.getLeft().accept(this);
        switch (node.getOp()) {
            case ADD:
                System.out.print(" + ");
                break;
            case SUB:
                System.out.print(" - ");
                break;
            case MUL:
                System.out.print(" * ");
                break;
            case DIV:
                System.out.print(" / ");
                break;
        }
    }

    @Override
    public void visitorConstantNode(AstConstantNode node) {
        System.out.print(" " + node.getValue() + " ");
    }
}
