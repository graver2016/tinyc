package com.laolang.graver.compiler.ast;

import lombok.Data;

/**
 * xxx.
 *
 * @author laolang
 * @version 0.1
 */
@Data
public class AstConstantNode implements AstNode {

    private Integer value;

    @Override
    public void accept(AstVisitor visitor) {
        visitor.visitorConstantNode(this);
    }
}
