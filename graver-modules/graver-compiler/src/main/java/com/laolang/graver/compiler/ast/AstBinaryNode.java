package com.laolang.graver.compiler.ast;

import lombok.Data;

/**
 * xxx.
 *
 * @author laolang
 * @version 0.1
 */
@Data
public class AstBinaryNode implements AstNode {

    private BinaryOperator op;
    private AstNode left;
    private AstNode right;


    @Override
    public void accept(AstVisitor visitor) {
        visitor.visitorBinaryNode(this);
    }
}
