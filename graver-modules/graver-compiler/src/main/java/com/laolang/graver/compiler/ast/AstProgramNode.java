package com.laolang.graver.compiler.ast;

import lombok.Data;

/**
 * xxx.
 *
 * @author laolang
 * @version 0.1
 */
@Data
public class AstProgramNode implements AstNode {

    private AstNode node;

    @Override
    public void accept(AstVisitor visitor) {
        visitor.visitorProgramNode(this);
    }
}
