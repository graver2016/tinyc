#!/bin/bash

# 获取当前脚本的绝对路径
script_path=$(readlink -f "$0")
# 获取当前脚本所在目录
script_dir=$(dirname "$script_path")

mvn clean install -Dmaven.test.skip
cp graver/target/graver-jar-with-dependencies.jar dist/graver.jar
cd dist
rm a.out
rm tmp.s
java -jar graver.jar -o tmp.s
clang tmp.s driver.c -o a.out
./a.out