package com.laolang.graver;

import com.laolang.graver.compiler.ast.AstPrinterVisitor;
import com.laolang.graver.compiler.ast.AstProgramNode;
import com.laolang.graver.compiler.ast.AstVisitor;
import com.laolang.graver.compiler.codegen.CodeGen;
import com.laolang.graver.compiler.lexer.Lexer;
import com.laolang.graver.compiler.parser.Parser;
import com.laolang.graver.compiler.token.Token;
import com.laolang.graver.compiler.token.TokenKind;
import lombok.extern.slf4j.Slf4j;
import org.testng.annotations.Test;

/**
 * xxx.
 *
 * @author laolang
 * @version 0.1
 */
@Slf4j
public class CommonTest {

    @Test
    public void testOne() {
        System.out.println("\tidiv %rdi");
    }

    @Test
    public void testLexer() {
        Lexer lexer = new Lexer("5 +a 1-3*4/2");
        do {
            lexer.nextToken();
            Token token = lexer.getCurrToken();
            log.info(token.toString());
        } while (TokenKind.EOF != lexer.getCurrToken().getKind());
    }

    @Test
    public void testParser() {
        Lexer lexer = new Lexer("5 + 1-3*4/2");
        Parser parser = new Parser(lexer);
        AstVisitor visitor = new AstPrinterVisitor();
        AstProgramNode program = parser.parse();
        program.accept(visitor);
    }

    @Test
    public void testCodeGen() {
        Lexer lexer = new Lexer("1+2+3+4*4*89-4*5*6/3");
        Parser parser = new Parser(lexer);
        AstVisitor visitor = new CodeGen();
        AstProgramNode program = parser.parse();
        program.accept(visitor);
    }
}
