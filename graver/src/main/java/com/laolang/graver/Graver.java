package com.laolang.graver;

import com.laolang.graver.compiler.Compiler;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

/**
 * xxx.
 *
 * @author laolang
 * @version 0.1
 */
@Slf4j
public class Graver {

    public static void main(String[] args) {
        log.info("graver is running...");
        Graver app = new Graver();
        app.defaultParse(args);
    }

    private void defaultParse(String[] args) {
        Options options = new Options();
        try {
            options.addOption("o", "out", true, "输出文件");

            CommandLine cmd = new DefaultParser().parse(options, args);
            boolean hasOut = cmd.hasOption("o");
            Compiler compiler = new Compiler();
            String text = "5 + 1 - 3 * 4 / 2";
            if (hasOut) {
                String outFile = cmd.getOptionValue("o");
                System.out.println("outfile:" + outFile);
                compiler.compile(text, outFile);
            } else {
                compiler.compile(text, null);
            }
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

}
